Read the file "Rapport_Verbockhaven_Giacomoni.pdf" if you want the theory  
Run the file  "Rapport_Verbockhaven_Giacomoni.ipynb" with notebook if you want to test the model  
Before running the scrip with notebook, make sure you have download the correct data :  
the website to download the data : https://chriswhong.com/open-data/foil_nyc_taxi/  
Then put the file in the folder "Ressources" and rename it : "trip_data_1.csv"  

## You are ready now, use the notebook to try the self-driving car !
